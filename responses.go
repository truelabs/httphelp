package httphelp

import (
	"compress/gzip"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"runtime"
	"strings"
)

func RespondJSON(w http.ResponseWriter, r *http.Request, data interface{}) {
	// compact JSON representation
	//b, err := json.Marshal(data)

	// pretty JSON representation
	bytes, err := json.MarshalIndent(data, "", " ")

	if err == nil {
		w.Header().Add("Content-Type", "application/json")
		w.Header().Set("Cache-Control", "no-store")
		// WARNING: tighten this up. It currently allows the web site to access the stitch API.
		w.Header().Set("Access-Control-Allow-Origin", "*")
		if strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
			w.Header().Add("Content-Encoding", "gzip")
			gz := gzip.NewWriter(w)
			defer gz.Close()
			gz.Write(bytes)
		} else {
			w.Write(bytes)
		}
	} else {
		log.Println("Error marshalling JSON.", err)
		RespondInternalServerError(w, r, err)
	}
}

func RespondInternalServerError(w http.ResponseWriter, r *http.Request, err error) {
	var buf = make([]byte, 10000)
	runtime.Stack(buf, false)
	log.Println("Internal Server Error. URL:", r.URL, ". Error:", err, "Call Stack:\n", string(buf))
	w.WriteHeader(http.StatusInternalServerError)
}

// message is intended to help out the developer making the bad request.
func RespondBadRequest(w http.ResponseWriter, message string) {
	log.Println("Bad Request:", message)
	w.WriteHeader(http.StatusBadRequest)
	io.WriteString(w, message+"\n")
}

func RespondNotFound(w http.ResponseWriter) {
	log.Println("Not found")
	w.WriteHeader(http.StatusNotFound)
}

func RespondForbidden(w http.ResponseWriter) {
	log.Println("Forbidden")
	w.WriteHeader(http.StatusForbidden)
}
