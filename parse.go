package httphelp

import (
	"fmt"
	"net/http"
	"strconv"
)

func ParseFloat64(s string) (float64, error) {
	return strconv.ParseFloat(s, 64)
}

func ParseFloat64FromRequest(r *http.Request, param string) (float64, error) {
	str := r.FormValue(param)
	if str == "" {
		return 0, fmt.Errorf("Parameter %v not set.", param)
	}
	return ParseFloat64(str)
}
