package httphelp

import (
	"log"
	"net/http"
	"net/http/httputil"
)

func LogRequestMethodAndURLHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}

func LogRequestDumpHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		dump, err := httputil.DumpRequest(r, true)
		if err == nil {
			log.Println(string(dump))
		} else {
			log.Println("ERROR LOGGING REQUEST:", err)
		}
		handler.ServeHTTP(w, r)
	})
}

func NoOpHandler(handler http.Handler) http.Handler {
	return handler
}

var RequestLoggerHandler = LogRequestMethodAndURLHandler
